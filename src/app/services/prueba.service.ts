import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

export interface Datos {
  id: Number,
  correo: String,
  select1: String,
  select2: String
}

export interface Select {
  id: Number,
  valor: String  
}
const API_URL: string = 'http://localhost:8000/api';

@Injectable({
  providedIn: 'root'
})
export class PruebaService {

  constructor() { }
}

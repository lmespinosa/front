import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable} from 'rxjs/internal/Observable';
import { tokenName } from '@angular/compiler';
import {map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})



export class DataApiService {
  datos: Observable<any>;  
  select: Observable<any>;
  
  constructor(private Http: HttpClient) { }
  /*
  headers : HttpHeaders = new HttpHeaders({
    "Content-Type" : "application/json",
    Authorization: tokenName;
  }

  )*/

  getSelectData(){
    const url_api ='http://127.0.0.1:8000/api/getSelect';
    return (this.select = this.Http.get(url_api));
  }
  getDatos(){
    const url_api ='http://127.0.0.1:8000/api/getDatos';
    return (this.datos = this.Http.get(url_api));
  }

  crearDatoPost(correoStr:string, select1Str:string, select2Str:string){
    const url_api ='http://127.0.0.1:8000/api/store';
    console.log(correoStr);

    //return this.Http.post(url_api dato,{headers: this.headers}).pipe(map(data) => data);
    //return this.Http.post<HttpResponse<Object>>(url_api,JSON.stringify(obj),{});
    return this.Http.post<any>(url_api,{correo:correoStr,select1:select1Str,select2:select2Str });
  }

}




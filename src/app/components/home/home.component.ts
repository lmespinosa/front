import { Component, OnInit } from '@angular/core';
import {SelectItem} from 'primeng/api';
import {SelectItemGroup} from 'primeng/api';
import {DataApiService} from 'src/app/services/data-api.service';
import { Data } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {UserData} from 'src/app/core/user-data';
import { ɵangular_packages_platform_browser_dynamic_platform_browser_dynamic_a } from '@angular/platform-browser-dynamic';

interface Datos{
  id: string;
  valor: string;
}

interface DatosUsuarios{
  correo:string;
  select1: string;
  select2: string;
  updated_at: string;
  created_at: string;
  id: string;
}


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  userModel = new UserData('','','');

  formulario = new FormGroup({
    email: new FormControl('',[
      Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]),    
    drop1: new FormControl(''),
    drop2: new FormControl('')
  }); 

  items1: SelectItem[];
  items2: Datos[];
  selectedCar: string;
  correo: string;
  showSelect: boolean;
  groupedCars: SelectItemGroup[];

  selectedValue1: Datos;
  selectedValue2: Datos;
  selectedValue: string = '';

  datosTable:DatosUsuarios[];

  datos:DatosUsuarios;
  
  constructor(private dataApi: DataApiService) {
    this.items1 = [
          {label:' - Seleccionar un valor - ', value:null},
          {label:'1', value:{id:1, name: '1'}},
          {label:'2', value:{id:2, name: '2'}},
          {label:'3', value:{id:3, name: '3'}},
          {label:'4', value:{id:4, name: '4'}},
      ];
  
    }
    
    isUno(){
      this.showSelect=false;
      if(this.selectedValue1){
        if(this.selectedValue1.id == "1"){
          
          this.dataApi.getSelectData().subscribe((datos) => this.items2=datos as Array<Datos>);
          console.log(this.items2);
          this.showSelect=true;
        }
      }
      
      return this.showSelect;
    }
    

  ngOnInit() { 
       
    this.dataApi.getDatos().subscribe((datos) => this.datosTable=datos as Array<DatosUsuarios>);  
  }

  guardarDatos(){
   
    this.dataApi.crearDatoPost(this.userModel.correo,this.selectedValue1.id,this.selectedValue2.valor).subscribe((datos) => console.log(datos));
    this.dataApi.getDatos().subscribe((datos) => this.datosTable=datos as Array<DatosUsuarios>);  
    
    
  }

}
